package in.silentsudo.autowiregenericservicecontroller.service;

public abstract class AbstractTransporterService {

    final public String supportedVersion() {
        return "1.0";
    }

    public abstract String transport();
}
