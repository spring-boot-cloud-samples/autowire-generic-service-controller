package in.silentsudo.autowiregenericservicecontroller.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("audioTransporterService")
public class AudioTransporterService extends AbstractTransporterService {
    @Override
    public String transport() {
        return "transporting audio details";
    }
}
