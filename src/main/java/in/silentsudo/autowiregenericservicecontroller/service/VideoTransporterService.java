package in.silentsudo.autowiregenericservicecontroller.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("videoTransporterService")
public class VideoTransporterService extends AbstractTransporterService {
    @Override
    public String transport() {
        return "transporting video details";
    }
}
