package in.silentsudo.autowiregenericservicecontroller.controller;

import in.silentsudo.autowiregenericservicecontroller.service.AudioTransporterService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/audio")
public class AudioController extends AbstractController<AudioTransporterService> {


    public AudioController(AudioTransporterService service) {
        super(service);
    }

    @GetMapping
    public String get() {
        return getService().transport();
    }
}
