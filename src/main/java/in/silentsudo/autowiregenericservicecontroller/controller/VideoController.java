package in.silentsudo.autowiregenericservicecontroller.controller;

import in.silentsudo.autowiregenericservicecontroller.service.VideoTransporterService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/video")
public class VideoController extends AbstractController<VideoTransporterService> {

    public VideoController(VideoTransporterService service) {
        super(service);
    }

    @GetMapping
    public String get() {
        return getService().transport();
    }
}
