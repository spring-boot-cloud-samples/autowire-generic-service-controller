package in.silentsudo.autowiregenericservicecontroller.controller;

import in.silentsudo.autowiregenericservicecontroller.service.AbstractTransporterService;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

public abstract class AbstractController<S extends AbstractTransporterService> {
    private final S service;

    public AbstractController(S service) {
        this.service = service;
    }

    public S getService() {
        return service;
    }


    @RequestMapping(path = "/version")
    public Map<String, String> controllerVersion() {
        return Map.of("type", getClass().getName(), "v", service.supportedVersion());
    }
}
