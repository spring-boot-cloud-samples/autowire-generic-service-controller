package in.silentsudo.autowiregenericservicecontroller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//https://stackoverflow.com/questions/69650667/autowiring-generict-service-in-controller-within-spring-boot
@SpringBootApplication
public class AutowireGenericServiceControllerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutowireGenericServiceControllerApplication.class, args);
    }

}
